The pyRIST API Reference
************************

The "rist" module
=================

.. automodule:: rist

Sender
^^^^^^

.. autoclass:: rist.Sender
   :members:
   :member-order: bysource

Receiver
^^^^^^^^

.. autoclass:: rist.Receiver
   :members:
   :member-order: bysource

Nack (IntEnum)
^^^^^^^^^^^^^^

.. autoclass:: rist.Nack
   :members:
   :member-order: bysource

Profile (IntEnum)
^^^^^^^^^^^^^^^^^

.. autoclass:: rist.Profile
   :members:
   :member-order: bysource

LogLevel (IntEnum)
^^^^^^^^^^^^^^^^^^

.. autoclass:: rist.LogLevel
   :members:
   :member-order: bysource

RecoveryMode (IntEnum)
^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: rist.RecoveryMode
   :members:
   :member-order: bysource

BufferBloatMode (IntEnum)
^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: rist.BufferBloatMode
   :members:
   :member-order: bysource

UrlParam (Enum)
^^^^^^^^^^^^^^^

.. autoclass:: rist.UrlParam
   :members:
   :member-order: bysource

Peer
^^^^

.. autoclass:: rist.Peer
   :members:
   :member-order: bysource

Out Of Band
^^^^^^^^^^^

.. autoclass:: rist.OobBlock
   :members:
   :member-order: bysource

Data
^^^^

.. autoclass:: rist.DataBlock
   :members:
   :member-order: bysource

RejectPeerException (Exception)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoexception:: rist.RejectPeerException
   :members:
   :member-order: bysource

ResultException (Exception)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoexception:: rist.ResultException
   :members:
   :member-order: bysource
   :exclude-members: act
