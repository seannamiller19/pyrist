# pyRIST. Copyright 2019-2020 Mad Resistor LLP. All right reserved.
# Author: Kuldeep Singh Dhaka <kuldeep@madresistor.com>

"""Wrap MPEG stream inside RIST stream

Usage:
  wrap-mpeg.py [--mpeg=<MPEG_URL>] [--rist=<RIST_URL>]
  wrap-mpeg.py (-h | --help)
  wrap-mpeg.py --version

Options:
  -m --mpeg=<MPEG_URL>     MPEG Stream URL    [default: localhost:8000]
  -r --rist=<RIST_URL>     RIST Receiver URL  [default: @localhost:8001]
"""

import rist
from docopt import docopt
import socket

args = docopt(__doc__)

def addr_port(s):
	a, p = s.split(':')
	return a, int(p)

MPEG_URL = args['--mpeg']
RIST_URL = args['--rist']

s = rist.Sender()
peer = s.peer_create({
	'address': RIST_URL
})

s.start()

MPEG_ADDR, MPEG_PORT = addr_port(MPEG_URL)
u = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
u.bind((MPEG_ADDR, MPEG_PORT))

try:
	while True:
		data, addr = u.recvfrom(10240)
		if len(data): s.data_write(data)
except: pass
